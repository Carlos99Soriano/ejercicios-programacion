#include<stdio.h>
int main(){
    float lad1,lad2, lad3, peri;
    printf(" \nPrograma para calcular el perimetro de un Triangulo Escaleno ");
    printf(" \nIngrese la medida del lado1: ");
    scanf("%f",&lad1);
    printf(" \nIngrese la medida del lado2: ");
    scanf("%f",&lad2);
    printf(" \nIngrese la medida del lado3: ");
    scanf("%f",&lad3);
    peri= lad1+lad2+lad3;// Es igual a suma de todos sus lados
    printf("\nEL Perimetro del Triangulo Escaleno es: %.2f ",peri);
    return 0;
}
