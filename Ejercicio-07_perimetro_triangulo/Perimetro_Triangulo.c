#include<stdio.h>
int main(){
    float ladEqui,ladIso1,ladIso2,ladEsca1,ladEsca2,ladEsca3, peri;
    int opc;
    printf("------------  Calculo de Perimetros de Triangulos  ------------");
    printf("\nA continuacion se muestran las opciones de los triangulospara calcular su perimetro ");
    printf("\n1.- Triangulo Equilatero");
    printf("\n2.- Triangulo Isosceles");
    printf("\n3.- Triangulo Escaleno");
    printf("\n�Que opcion desea elegir?: ");
    scanf("%i",&opc);
    switch(opc){
        case 1:
            printf("\nUsted acaba de elegir el Triangulo Equilatero");
            printf(" \nIngrese la medida de un lado: ");
            scanf("%f",&ladEqui);
            peri=ladEqui*3;// Es igual a suma de todos sus lado lad+lad+lad
            printf("\nEL Perimetro del Triangulo Equilatero es: %.2f ",peri);
            break;
        case 2:
            printf("\nUsted acaba de elegir el Triangulo Isosceles");
            printf(" \nIngrese la medida de un lado: ");
            scanf("%f",&ladIso1);
            printf(" \nIngrese la medida de otro lado(equivale por 2 lados): " );
            scanf("%f",&ladIso2);
            peri=(ladIso2*2)+ladIso1;// Es igual a suma de todos sus lado (lad+lad)+lad
            printf("\nEL Perimetro del Triangulo Isosceles es: %.2f ",peri);
            break;
        case 3:
            printf("\nUsted acaba de elegir el Triangulo Escaleno");
            printf(" \nIngrese la medida del lado1: ");
            scanf("%f",&ladEsca1);
            printf(" \nIngrese la medida del lado2: ");
            scanf("%f",&ladEsca2);
            printf(" \nIngrese la medida del lado3: ");
            scanf("%f",&ladEsca3);
            peri= ladEsca1+ladEsca2+ladEsca3;// Es igual a suma de todos sus lados
            printf("\nEL Perimetro del Triangulo Escaleno es: %.2f ",peri);
            break;
        default:
            printf("\nOpcion Invalida");
            break;
    }

    return 0;
}

