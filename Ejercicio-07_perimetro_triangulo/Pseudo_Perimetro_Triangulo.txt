Inicio Perimetros
	Variable ladEqui es Numero Real
	Variable ladIso1 es Numero Real
	Variable ladIso2 es Numero Real
	Variable ladEsca1 es Numero Real
	Variable ladEsca2 es Numero Real
	Variable ladEsca3 es Numero Real
	Variable opc es Numero Real
	Variable peri es Numero Real
	Escribir "La opciones de tipos de triangulos que puedes calcular su perimetro"
	Escribir "1.- Triangulo Equilatero"
	Escribir "2.- Triangulo Isosceles"
	Escribir "3.- Triangulo Escaleno"
	Escribir "�Que opcion deseas: "
	Leer opc
        Segun opc Hacer
		1:
			Escribir "Programa para calcular el perimetro de un Triangulo Equilatero "
			Escribir "Ingresa la medida den lado"
			Leer ladEqui;
			peri = ladEqui * 3
			Escribir "El perimetro del Triangulo Equilatero es: ",peri
		
		2:
			Escribir "Programa para calcular el perimetro de un Triangulo Isosceles "
			Escribir "Ingresa la medida de lado"
			Leer ladIso1;
			Escribir "Ingresa la medida de otro lado (equivale por 2 lados)"
			Leer ladIso2;
			peri = (ladIso2 * 2)+ ladIso!
			Escribir "El perimetro del Triangulo Isosceles es: ",peri
		3:
			Escribir "Programa para calcular el perimetro de un Triangulo Escaleno"
			Escribir "Ingresa la medida de lado"
			Leer ladEsca1;
			Escribir "Ingresa la medida de lado "
			Leer ladEsca2;
			Escribir "Ingresa la medida de lado"
			Leer ladEsca3;
			peri = ladEsca1+ladEsca2+ladEsca3
			Escribir "El perimetro del Triangulo Escalenoes: ",peri

    		De otro modo: 
				Escribir "Opcion Invalida"
	FinSegun
Fin