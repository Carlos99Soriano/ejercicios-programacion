#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define False 0
#define True 1
int main(){
    srand(time(NULL)); //Genera numeros distintos al tiempo de ejecucion
    int aleatorio = 1+ rand()% (101-1);//Funcion de numeros aleatorios RANDOM
    int juegoTerminado= False;
    int intentos = 5;
    int numero;
    printf("Adivina un numero entre 1 y 100 tienes 5 oportunidades");
    while(juegoTerminado == False){
        printf("\nIngresa un numero: ");
        scanf("%i",&numero);
        if(numero== aleatorio){
            printf("\nGANASTE");
            printf("\nEl numero es: %i ",aleatorio);
            juegoTerminado = True;
        }else{
            printf("\nEse no es el numero");
        }
        intentos--;
        if((intentos>0) && (juegoTerminado == False)){
            printf("\nTe quedan %i intento(s)",intentos);
        }else{
            printf("\nPERDISTE");
            printf("\nEl numero es: %i ",aleatorio);
            juegoTerminado = True;
        }
    }
    return 0;
}
