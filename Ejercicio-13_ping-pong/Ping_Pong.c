#include<stdio.h>
int main(){
    //programa que imprima los n�meros del 1 al 100, pero aplicando las siguientes reglas.
        //Regla 1: Cuando el n�mero sea divisible entre 3, en vez del n�mero debe escribir "ping"
        //Regla 2: Cuando el n�mero sea divisible entre 5, en vez del n�mero debe escribir "pong"
        //Regla 3: Cuando el n�mero sea divisible entre 3 y tambi�n divisible entre 5, en vez del n�mero debe escribir "ping-pong"
    int i;
    for(i=1;i<=100;i++){
        if((i%3==0)&& (i%5!=0)){
            printf("\n Ping");
        }if((i%5==0)&& (i%3!=0)){
            printf("\n Pong");
        }if((i%3==0)&& (i%5==0)){
            printf("\n Ping-Pong");
        }if((i%3!=0)&&(i%5!=0)){
            printf("\n %i ",i);
        }
    }
    return 0;
}
