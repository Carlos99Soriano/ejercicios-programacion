#include<stdio.h>
int main(){
    float lad1,lad2, peri;
    printf(" \nPrograma para calcular el perimetro de un Triangulo Isosceles ");
    printf(" \nIngrese la medida de un lado: ");
    scanf("%f",&lad1);
     printf(" \nIngrese la medida de otro lado (equivalen 2 lados): ");
    scanf("%f",&lad2);
    peri=(lad2*2)+lad1;// Es igual a suma de todos sus lado (lad+lad)+lad
    printf("\nEL Perimetro del Triangulo Isosceles es: %.2f ",peri);
    return 0;
}