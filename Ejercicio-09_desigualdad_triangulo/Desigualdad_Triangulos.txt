#include<stdio.h>
int main(){
    float A, B, C;
    //Programa que determina si un triangulo existe por la medida de sus lados
    printf("\nIngrese el primer lado del triangulo: ");
    scanf("%f",&A);
    printf("\nIngrese el segundo lado del triangulo: ");
    scanf("%f",&B);
    printf("\nIngrese el tercer lado del triangulo: ");
    scanf("%f",&C);
    //Si la suma de dos lados lados del triangulo es mayor al tercero, es un triangulo
    if(((A+B)>C) && ((A+C)>B) && ((B+C)>A) ){
        printf("\nEs posible formar el triangulo");
    }else{
        printf("\nNo es posible formar el triangulo");
    }
    return 0;
}