﻿# Ejercicios del curso Introducción a la programación
En este curso se encuentran los resultados de los ejercicios por cada actividad.
## Descripción
En este curso se abordaràn una breve introducción a las técnicas 
programación estructurada, se describe los conceptos elementales tales 
como:Estructuras de control de flujo, Variables, Constantes y Operadores. 
Se aprende lo básico y estarás listo para encontrar nuevos retos que resolver.
## Autor
*Soriano Bonilla Carlos Ivan*
### Contacto
cisoriano99@gmail.com
